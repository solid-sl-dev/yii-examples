<?php

namespace modules\user\controllers\backend;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use modules\user\models\User;
use modules\user\models\UserSearch;
use backend\controllers\BackController;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BackController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->loadDefaultValues();
        $model->verified = true;
        $model->scenario = User::SCENARIO_WITH_PASSWORD;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'activeTab' => Yii::$app->request->get('tab'),
            ]);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);
        $model->reverseActive();

        return $this->redirect(['index']);
    }

    /**
     * @param bool $active
     * @return array
     */
    public function actionActivateList($active = true)
    {
        $ids = Yii::$app->request->post('ids');
        $success = false;

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $model = $this->findModel((int) $id);
                if (!empty($model)) {
                    $success = $model->setActive($active);
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'success' => $success,
        ];
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteList()
    {
        $ids = Yii::$app->request->post('ids');
        $success = false;

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $model = $this->findModel((int) $id);
                if (!empty($model)) {
                    $success = $model->delete();
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'success' => $success,
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionDeleteImage($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'success' => $this->findModel($id)->deleteImage(),
        ];
    }

    /**
     * @param $id
     * @return User|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
