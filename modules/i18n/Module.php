<?php

namespace modules\i18n;

use Yii;
use yii\base\Module as BaseModule;

/**
 * translation module definition class
 */
class Module extends BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'modules\translation\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::configure($this, require(__DIR__ . '/config/params.php'));
    }
}
